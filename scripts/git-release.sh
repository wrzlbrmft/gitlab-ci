#!/usr/bin/env sh
# branch candidates: prefer right-most over left-most (a < b < c)
DEVELOP_BRANCHES="develop"
RELEASE_BRANCHES="master main release"

if [ ! -d .git ]; then
    echo "* error: not a git repository"
    exit 1
fi

# find develop branch
for i in ${DEVELOP_BRANCHES}; do
    if [ -n "$(git branch --list $i)" ]; then
        DEVELOP=$i
    fi
done
if [ -z "${DEVELOP}" ]; then
    echo "* error: no develop branch (${DEVELOP_BRANCHES})"
    git branch
    exit 1
fi

# find release branch
for i in ${RELEASE_BRANCHES}; do
    if [ -n "$(git branch --list $i)" ]; then
        RELEASE=$i
    fi
done
if [ -z "${RELEASE}" ]; then
    echo "* error: no release branch (${RELEASE_BRANCHES})"
    git branch
    exit 1
fi

echo "* release: ${DEVELOP} -> ${RELEASE}"
echo
echo "  $(git log -1 --format=%s)"
echo

echo "> fetch"
git fetch -p -q

echo "> pull ${RELEASE}"
git checkout ${RELEASE} -q
git pull -q

echo "> pull ${DEVELOP}"
git checkout ${DEVELOP} -q
git pull -q

echo "> merge ${RELEASE} into ${DEVELOP}"
git merge ${RELEASE} -q

echo "> merge ${DEVELOP} into ${RELEASE}"
git checkout ${RELEASE} -q
git merge ${DEVELOP}

echo "> push ${RELEASE}"
git push -q

[ -f VERSION ] && echo && echo "* version: $(cat VERSION)"
echo
echo "* done."
